# Fonctions

## le dossier sources

### le sous-dossier database

#### le fichier create_db.py

Import de la bibliothèque SQLite :
    - Module sqlite3 pour interagir avec les bases de données SQLite.

Connexion à la base de données :
    Une connexion à la base de données est établie en utilisant la fonction connect() de sqlite3. Le chemin du fichier de la base de données est spécifié comme ../users.db.

Création du curseur :
    Un objet curseur (c) est créé pour exécuter des commandes SQL sur la base de données.

Création de la table lndb :
    Une table nommée lndb est créée si elle n'existe pas déjà.
    La table lndb comprend les colonnes suivantes :
        id, last_name, name, classe : Informations sur l'étudiant.
        monday_vote, tuesday_vote, wednesday_vote, thursday_vote, weekend_vote : Votes de l'étudiant pour chaque jour de la semaine.
        monday_hour, tuesday_hour, wednesday_hour, thursday_hour, weekend_hour : Heures auxquelles l'étudiant a voté pour chaque jour de la semaine.

    Création de la table professeurs :
        Une table nommée professeurs est créée si elle n'existe pas déjà.
        La table professeurs comprend les colonnes suivantes :
            id, last_name, name : Informations sur le professeur.
            classes : Classes enseignées par le professeur.

    Validation des modifications et fermeture de la connexion :
        Les modifications sont validées en appelant commit() sur l'objet de connexion.
        La connexion à la base de données est fermée en appelant close().

#### le fichier sql_tools.py

    Imports : 
    - Module sqlite3 pour interagir avec les bases de données SQLite.

    Fonctions : 
    
    sql_execute(code, request) :
        Cette fonction exécute une commande SQL spécifiée par code.
        Si request est spécifié comme "fetchone", la fonction récupère et renvoie une seule ligne de résultat.
        Si request est spécifié comme "fetchall", la fonction récupère et renvoie toutes les lignes de résultat.
        La connexion à la base de données est établie, la commande est exécutée, les résultats sont récupérés si nécessaire, puis la connexion est fermée avant de renvoyer les résultats.

    is_in_db(user_id, table) :
        Cette fonction vérifie si un utilisateur avec l'ID spécifié existe dans la table spécifiée de la base de données.
        La commande SQL sélectionne tous les IDs de la table spécifiée.
        Si l'ID de l'utilisateur est trouvé dans les résultats, la fonction renvoie True, sinon elle renvoie False.

    update_vote(edited_id, color, day) :
        Cette fonction met à jour le vote d'un utilisateur pour un jour spécifique avec une nouvelle couleur.
        Les noms de colonnes pour le vote et l'heure pour le jour spécifié sont déterminés en fonction de day.
        Une commande SQL UPDATE est exécutée pour mettre à jour les colonnes spécifiées avec la nouvelle couleur et l'heure actuelle.

    add_student(id, new_lastname, new_name, new_class) :
        Cette fonction ajoute un nouvel étudiant à la base de données avec l'ID, le nom, le nom de famille et la classe spécifiés.
        Une commande SQL INSERT est exécutée pour insérer les valeurs dans la table lndb.

    get_vote_per_class(wanted_class, day) :
        Cette fonction récupère le nombre de votes de chaque couleur pour une classe spécifique et un jour donné.
        La couleur de chaque vote est comptée et stockée dans un dictionnaire, puis renvoyée.

    get_class(edited_id) :
        Cette fonction récupère la classe d'un utilisateur spécifié par son ID.

    add_teacher(user_id, name, last_name, list_of_classes) :
        Cette fonction ajoute un nouvel enseignant à la base de données avec l'ID, le nom, le nom de famille et la liste des classes spécifiés.
        La liste des classes est convertie en chaîne à l'aide de la fonction list_to_string du module tools.

    get_list_of_classes(teacher_id) :
        Cette fonction récupère la liste des classes enseignées par un enseignant spécifié par son ID.

    get_time_for_vote(user_id, day) :
        Cette fonction récupère l'heure à laquelle un utilisateur a voté pour un jour spécifique.

    get_color_voted(user_id, day) :
        Cette fonction récupère la couleur pour laquelle un utilisateur a voté pour un jour spécifique.

    delete_vote_end_day() :
        Cette fonction supprime les votes et les heures de vote pour le premier jour de la semaine (lundi) afin de réinitialiser les votes pour la nouvelle semaine.


### le sous-dossier scripts

#### le fichier already_voted.py
    Imports :
        - Flask
        - Blueprint est importé pour permettre de définir des routes modulaires.
        - request, render_template, redirect, et url_for sont des fonctions de Flask: gestion des requêtes HTTP, des rendus de templates HTML, et de la redirection d'URLs.
        - login_required et current_user: gestion de sessions utilisateur fournie par Flask-Login

    Création du Blueprint :
        Un objet Blueprint nommé already_voted_path est créé pour gérer les routes liées aux votes déjà effectués.

    Routes :
        La première route (/already_voted) est associée à la fonction show_already_voted(). Cette route est accessible uniquement via la méthode HTTP GET. Elle est protégée par le décorateur 
        login_required, ce qui signifie que seul un utilisateur authentifié peut y accéder. La fonction récupère des paramètres de requête day et time_left, puis rend un template HTML appelé 
        already_voted.html, en passant ces paramètres à ce template.
        
        La deuxième route (/already_voted_button) est associée à la fonction action_buttons_already_voted(). Cette route est accessible uniquement via la méthode HTTP POST. Elle est également 
        protégée par login_required. La fonction récupère le bouton feeling_button envoyé dans le formulaire. Si ce bouton est une chaîne de caractères (impliquant qu'un sentiment a été 
        sélectionné), l'utilisateur est redirigé vers une route spécifique (show_feeling) en fonction du jour sélectionné. Sinon, l'utilisateur est redirigé vers une autre route (show_days), 
        avec un paramètre time_left fixé à 10.


#### le fichier class feeling.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required et current_user: gestion de sessions utilisateur fournie par Flask-Login
        - sql_tools et tools sont importés depuis les modules database et scripts respectivement (autres fichiers).

    Création du Blueprint :
        Un objet Blueprint nommé feeling_path est créé pour gérer les routes liées aux sentiments des étudiants par classe.

    Routes :
        La première route (/classe) est associée à la fonction show_feeling(). Cette route est accessible uniquement via la méthode HTTP GET et nécessite une authentification. La fonction récupère le jour à partir des arguments de requête. Ensuite, elle vérifie si l'utilisateur actuel est un étudiant, et si oui, elle détermine combien de temps il reste avant qu'il puisse voter à nouveau. En fonction du temps restant, l'utilisateur est redirigé vers différentes pages.
        
        La deuxième route (/class_button_retour) est associée à la fonction return_to_menu(). Elle est accessible uniquement via la méthode HTTP POST et nécessite une authentification. Cette route redirige simplement l'utilisateur vers une autre page (show_days).
        
        La troisième route (/arrow_pressed) est associée à la fonction arrow_action(). Elle est accessible uniquement via la méthode HTTP POST et nécessite une authentification. Cette route gère les actions liées aux flèches de navigation. Selon la flèche pressée, l'utilisateur est redirigé vers le jour précédent ou suivant pour afficher les résultats.

    Logique de traitement :
        Les fonctions effectuent diverses opérations, telles que la récupération de données à partir de la base de données, la manipulation des jours et des résultats, et la gestion de la navigation entre les jours.

#### le fichier connection.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required: gestion de sessions utilisateur fournie par Flask-Login
        - requests, json, et urllib.parse: effectuer des requêtes HTTP et manipuler les données JSON.
        - user et tools sont importés depuis le module scripts, et sql_tools est importé depuis le module database (autres fichiers).

    Définition des fonctions utilitaires :
        - Traitement des réponses de l'API Ecole Directe
        - Extraction des informations spécifiques telles que le nom, le prénom, la classe, etc.
        Ces fonctions incluent get_response_api_ecole_directe, check_connection_info, get_ecole_directe_name_last_name_classe, get_ecole_directe_name_last_name_last_name_prof, get_ecole_directe_id, et get_ecole_directe_classes_teacher.

    Définition des routes :
        Une seule route est définie (/check_login) associée à la fonction check_login. Cette route est accessible uniquement via la méthode HTTP POST. Elle est destinée à vérifier les informations de connexion fournies par l'utilisateur.

    Fonction check_login :
        Cette fonction récupère les informations de connexion (login et password) soumises par le formulaire.
        Elle appelle la fonction get_response_api_ecole_directe pour obtenir la réponse de l'API Ecole Directe.
        En fonction de la réponse de l'API, la fonction vérifie si la connexion est réussie ou non : 
        - Si la connexion est réussie, elle récupère les informations de l'utilisateur, les stocke dans la base de données si nécessaire, crée une instance d'utilisateur, la connecte et redirige vers la page suivante (show_days).
        - Si la connexion échoue, elle redirige vers la page de connexion avec un message d'erreur.

#### le fichier data.py

    Fichiers qui répertorie différents dictionnaires comme les jours, les couleurs, etc

#### le fichier days.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required et current_user: gestion de sessions utilisateur fournie par Flask-Login
        Deux fonctions sont importées depuis les modules scripts.tools et database.sql_tools (autres fichiers)

    Création du Blueprint :
        Un objet Blueprint nommé days_path est créé pour gérer les routes liées aux jours et aux classes dans l'application.

    Définition des routes :
        La première route (/days) est associée à la fonction show_days(). Cette route est accessible uniquement via la méthode HTTP GET et nécessite une authentification. Elle rend un template HTML (day_menu.html) qui présente tous les jours disponibles aux étudiants ou les classes disponibles aux enseignants.
        
        Les routes suivantes (/days_button_clicked, /classe_button_clicked, /days_classe_button) sont accessibles via la méthode HTTP POST et nécessitent également une authentification. Elles gèrent les actions des utilisateurs lorsqu'ils cliquent sur des boutons pour sélectionner un jour ou une classe, et effectuent les redirections appropriées en fonction de diverses conditions.

    Logique de traitement :
        Les fonctions action_button_clicked(), action_classe_clicked(), et action_day_button() traitent les données envoyées par les utilisateurs via les formulaires.
        Elles effectuent différentes actions en fonction des informations reçues, telles que vérifier si un utilisateur a déjà voté pour un jour donné, récupérer les couleurs associées aux votes des classes, ou naviguer vers une autre page en fonction des sélections de l'utilisateur.

#### le fichier revote.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required et current_user: gestion de sessions utilisateur fournie par Flask-Login
        Les modules sql_tools et tools sont importés depuis les dossiers database et scripts (autres fichiers)

    Création du Blueprint :
        Un objet Blueprint nommé revote_path est créé pour gérer les routes liées au revote dans l'application.

    Définition des routes :
        La première route (/revote) est associée à la fonction show_revote(). Cette route est accessible uniquement via la méthode HTTP GET et nécessite une authentification. Elle récupère le jour à partir des arguments de requête, vérifie si l'utilisateur peut voter à nouveau pour ce jour, récupère la couleur du vote précédent, et rend un template HTML (revote.html) avec la couleur du vote précédent.
        
        La deuxième route (/revote_button) est associée à la fonction action_button(). Cette route est accessible uniquement via la méthode HTTP POST et nécessite une authentification. Elle gère les actions de l'utilisateur lorsqu'il clique sur les boutons de revote ou de sentiment, et met à jour la base de données en conséquence.

    Logique de traitement :
        La fonction show_revote() vérifie si l'utilisateur peut voter à nouveau pour un jour spécifique et récupère la couleur de son vote précédent.
       
        La fonction action_button() gère les actions de l'utilisateur lorsqu'il souhaite revoter ou exprimer un nouveau sentiment pour un jour donné. Elle met à jour la base de données en conséquence et redirige l'utilisateur vers la page appropriée.


#### le fichier thanks.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required: gestion de sessions utilisateur fournie par Flask-Login

    Création du Blueprint :
        Un objet Blueprint nommé thanks_path est créé pour gérer les routes liées aux remerciements après le vote dans l'application.

    Définition des routes :
        La première route (/thanks_vote) est associée à la fonction show_thanks(). Cette route est accessible uniquement via la méthode HTTP GET et nécessite une authentification. Elle récupère le jour à partir des arguments de requête et rend un template HTML (thanks.html) pour afficher la page de remerciement.
        
        La deuxième route (/thanks_vote_button) est associée à la fonction action_button(). Cette route est accessible uniquement via la méthode HTTP POST et nécessite une authentification. Elle gère l'action de l'utilisateur lorsqu'il clique sur le bouton pour revenir à la sélection de sentiment ou de jour, et redirige l'utilisateur en conséquence.

    Logique de traitement :
        La fonction show_thanks() récupère le jour à partir des arguments de requête et rend un template HTML qui affiche la page de remerciement avec le jour spécifié.
        
        La fonction action_button() vérifie si l'utilisateur a cliqué sur le bouton pour sélectionner un sentiment. Si c'est le cas, elle redirige vers la page pour sélectionner un sentiment pour le jour spécifié. Sinon, elle redirige vers la page principale pour sélectionner un jour.

#### le fichier tools.py

    Imports : 
        - Datetime pour la date et l'heure
        - Ast pour manipuler le code source python
        Les modules sql_tools et data sont importés

    get_order_days() :
        Cette fonction renvoie une liste des cinq prochains jours, à partir d'aujourd'hui.
        Elle utilise le module datetime pour obtenir la date actuelle, puis détermine le jour de la semaine.
        Si c'est le week-end, la fonction ajuste le jour de la semaine en conséquence et crée une nouvelle liste des jours à partir de cet ajustement.
        La liste résultante est renvoyée.

    already_voted(user_id, day) :
        Cette fonction vérifie si un utilisateur a déjà voté pour un jour spécifique.
        Elle utilise une fonction get_time_for_vote de sql_tools pour obtenir le temps du dernier vote de l'utilisateur pour ce jour.
        Si un vote a été trouvé, la fonction calcule la différence de temps depuis ce vote jusqu'à maintenant, en heures, et la renvoie. Sinon, elle renvoie None.

    day_exists(day) :
        Cette fonction vérifie si un jour donné existe dans la liste des jours obtenue à partir de get_order_days().
        Elle renvoie True si le jour existe dans la liste, sinon elle renvoie False.

    is_student(api_response) :
        Cette fonction vérifie si l'utilisateur associé à la réponse de l'API Ecole Directe est un étudiant.
        Elle renvoie True si le type de compte est "E" (pour "Élève"), sinon elle renvoie False.

    list_to_string(list_to_convert) :
        Cette fonction convertit une liste en chaîne de caractères en utilisant la fonction str() de Python.
        Elle renvoie la chaîne de caractères résultante.

    str_to_list(str_to_convert) :
        Cette fonction convertit une chaîne de caractères spéciale (probablement produite par list_to_string()) en liste en utilisant la fonction ast.literal_eval() pour évaluer l'expression Python contenue dans la chaîne.
        Elle renvoie la liste résultante.

    gradient_color(vote_dict) :
        Cette fonction calcule une couleur de dégradé basée sur un dictionnaire contenant les votes des étudiants.
        Elle utilise les poids de couleur définis dans les dictionnaires colors_weight et reverse_colors_weight pour déterminer la couleur appropriée en fonction de la moyenne pondérée des votes.
        Elle renvoie la couleur calculée.

#### le fichier user.py 

    Imports :
        - Imports de flask_login le module UserMixin

    Création de classe :
    - User :
        Cette classe hérite de UserMixin fournie par Flask-Login, ce qui lui donne des fonctionnalités supplémentaires pour gérer les utilisateurs.
        Le constructeur __init__ initialise les attributs de l'utilisateur, notamment son ID, son statut d'étudiant et ses classes.
        Les attributs de l'utilisateur sont :
            id : l'identifiant de l'utilisateur.
            is_student : un booléen indiquant si l'utilisateur est un étudiant ou non.
            classes : une liste des classes auxquelles l'utilisateur est associé.
    -Config :
        Cette classe est une classe de configuration Flask standard.
        Elle définit une variable de classe SCHEDULER_API_ENABLED avec la valeur True.
        Cette variable contrôle si l'API du planificateur est activée ou désactivée.

#### le fichier vote.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - login_required et current_user: gestion de sessions utilisateur fournie par Flask-Login
        - Les modules day_exists et update_vote sont importés depuis tools et sql_tools.

    Création du Blueprint :
        Un objet Blueprint nommé vote_path est créé pour gérer les routes liées au vote dans l'application.

    Définition des routes :
        La première route (/vote) est associée à la fonction vote(). Cette route est accessible uniquement via la méthode HTTP GET et nécessite une authentification. Elle permet à l'utilisateur de voter pour un jour spécifique en affichant un formulaire de vote avec le jour spécifié.
        
        La deuxième route (/get_vote) est associée à la fonction get_result_vote(). Cette route est accessible uniquement via la méthode HTTP POST et nécessite une authentification. Elle gère la soumission du vote de l'utilisateur, met à jour la base de données avec le choix de couleur du vote, et redirige vers la page de remerciement après le vote.

    Logique de traitement :
        La fonction vote() récupère le jour à partir des arguments de requête et vérifie si le jour existe. Si le jour n'existe pas, l'utilisateur est redirigé vers la page principale pour sélectionner un jour. Sinon, un formulaire de vote est rendu avec le jour spécifié.
        
        La fonction get_result_vote() gère la soumission du vote de l'utilisateur en vérifiant si le bouton d'envoi a été cliqué. Si c'est le cas, elle récupère la couleur et le jour du vote à partir des données du formulaire, met à jour la base de données avec ces informations, puis redirige vers la page de remerciement. Sinon, l'utilisateur est redirigé vers la page principale pour sélectionner un jour.


### le fichier main.py

    Imports :
        - Imports de base de Flask (Blueprint, render_template, redirect, url_for, request)
        - LoginManager et logout_user depuis flask_login pour gérer l'authentification des utilisateurs.
        - APScheduler depuis flask_apscheduler pour la planification des tâches.
        -  Modules nécessaires depuis les scripts et la base de données de l'application.

    Création de l'application Flask :
        - Une instance de l'application Flask est créée avec Flask(__name__).
        - Une clé secrète est définie pour l'application Flask avec app.config['SECRET_KEY'] pour sécuriser les sessions utilisateur.

    Configuration du gestionnaire de connexion :
        - Un gestionnaire de connexion (LoginManager) est créé et initialisé avec l'application Flask.
        - La vue de connexion par défaut est définie sur "start".

    Planification des tâches :
        - La configuration de l'application est mise à jour à partir de la classe Config définie dans le module user.
        - Un planificateur de tâches (APScheduler) est initialisé et démarré pour exécuter des tâches planifiées.

    Fonction de chargement de l'utilisateur :
        Une fonction load_user est définie pour charger l'utilisateur à partir de son ID.
        - Si l'utilisateur est un étudiant, les classes sont définies sur None. 
        - Sinon, les classes de l'utilisateur sont récupérées à partir de la base de données. Un objet User est retourné avec les informations de l'utilisateur.

    Route principale de l'application :
        La racine de l'application ('/') est associée à la fonction start(). Cette fonction est appelée lorsque l'utilisateur accède à la page principale de l'application.
        Si un utilisateur est connecté, il est déconnecté. Ensuite, si un message d'erreur est présent dans les arguments de requête, il est récupéré et affiché sur la page de connexion.
        La page de connexion est rendue avec le message d'erreur éventuel.

    Enregistrement des Blueprints :
        Les différentes routes de l'application sont enregistrées en associant les Blueprints aux différentes parties de l'application, telles que la connexion, les jours, le vote, les remerciements, les ressentis de classe, les votes déjà effectués et le revote.

    Tâche planifiée :
        Une tâche planifiée est ajoutée pour exécuter la fonction scheduled_task() tous les jours à 23h59, qui supprime les votes de la journée précédente de la base de données.

    Exécution de l'application :
        L'application est exécutée en mode debug si le script est exécuté directement.


### les sous-dossiers Static et template

    L'ensemble des fichiers sont écrits en css, en html et définissent les fenêtres et leurs affichages.
    Chaque fichier css a un son fichier HTML associé


### le fichier tests.py

    Ce fichiers teste chacune des fonctions du programme à l'aide de pytest



