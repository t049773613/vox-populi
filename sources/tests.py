import pytest
import datetime as dt
from scripts import tools
from database import sql_tools
from scripts import connection
from scripts import data


@pytest.fixture
def current_day_number():
    """
        Créer une liste pour mapper les nombres de jours aux noms de jours
    """
    return dt.datetime.now().weekday()

@pytest.fixture
def test_string():
    return '[1, 2]'

@pytest.fixture
def test_list():
    return [1, 2]

@pytest.fixture
def test_right_response():
    return {'code': 200,\
            'data' : {'accounts' : [{'typeCompte' : 'E',\
                                     'prenom' : 'good_name',\
                                        'nom' : 'good_surname',\
                                        'id' : 0,
                                        'profile' : {'classe' : {'code' : "good_class"},\
                                                     'classes' : [{"code" : 1}, {"code" : 2}]}}]}}

@pytest.fixture
def test_wrong_response():
    return {'code': 0}



# TOOLS

def test_get_order_days(current_day_number):
    if current_day_number in [4,5,6]:
        assert tools.get_order_days() == data.friday_day_names
    elif current_day_number == 3:
        assert tools.get_order_days() == data.thursday_day_names
    elif current_day_number == 2:
        assert tools.get_order_days() == data.wednesday_day_names
    elif current_day_number == 1:
        assert tools.get_order_days() == data.tuesday_day_names
    elif current_day_number == 0:
        assert tools.get_order_days() == data.monday_day_names

def test_day_exists():
    day = "lundi"
    assert tools.day_exists(day) == True

def test_is_student(test_right_response):
    assert tools.is_student(test_right_response) == True

def test_list_to_string(test_string, test_list):
    assert tools.list_to_string(test_list) == test_string

def test_str_to_list(test_string, test_list):
    assert tools.str_to_list(test_string) == test_list




# CONNECTION

def test_check_connection_info(test_right_response, test_wrong_response):
    assert connection.check_connection_info(test_right_response) == True
    assert connection.check_connection_info(test_wrong_response) == False

def test_get_ecole_directe_name_last_name_classe(test_right_response):
    assert connection.get_ecole_directe_name_last_name_classe(test_right_response) == ('good_name', 'good_surname', 'good_class')

def test_get_ecole_directe_id(test_right_response):
    assert connection.get_ecole_directe_id(test_right_response) == 0

def test_get_ecole_directe_classes_teacher(test_right_response, test_list):
    assert connection.get_ecole_directe_classes_teacher(test_right_response) == test_list
