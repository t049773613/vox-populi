import datetime as dt
import ast
import time
from scripts.data import *
from database import sql_tools


def get_order_days():
    """
        Renvoie une liste des cinq prochains jours, à partir d'aujourd'hui
    """
    # Obtenir la date d'aujourd'hui
    today = dt.datetime.now()
    day_number = today.weekday()
    day_names = ["lundi", "mardi", "mercredi", "jeudi", "weekend"]
    if day_number in [5, 6]:  # Si samedi ou dimanche
        day_number = 4
    # Créer une liste pour mapper les nombres de jours aux noms de jours
    new_liste_jour = day_names[day_number:] + day_names[:day_number]
    return new_liste_jour


def already_voted(user_id, day):
    """function that check if we have already voted for a specific day"""

    # first get the time when we vote for a day
    result = sql_tools.get_time_for_vote(user_id, day)

    if result is not None:

        difference = time.time() - result
        difference = difference / 3600

        return difference

    else:
        return None


def day_exists(day):
    if day in get_order_days():
        return True
    else:
        return False


def is_student(api_response):
    data_json = api_response
    list_info = data_json['data']['accounts'][0]
    return list_info['typeCompte'] == 'E'


def list_to_string(list_to_convert):
    """function that takes care of converting a list to a str"""

    result_str = str(list_to_convert)

    return result_str


def str_to_list(str_to_convert):
    """function that takes care of converting a special str into a list"""

    result_list = ast.literal_eval(str_to_convert)

    return result_list


def gradient_color(vote_dict):
    sum = 0
    total_students = 0
    for key in vote_dict:
        total_students += vote_dict[key]
        sum += reverse_colors_weight[key] * vote_dict[key]
    if total_students == 0:
        return 'blue'
    else:
        return colors_weight[round(sum / total_students)]
