from flask import Blueprint, request, render_template, redirect, url_for
from flask_login import login_required, current_user
from scripts.tools import already_voted, get_order_days, gradient_color
from database import sql_tools

# create the Blueprint that allows to connect the root with the main app
days_path = Blueprint('days_path', __name__)


@days_path.route('/days')
@login_required
def show_days():
    """function that is called when we are in the specify path"""

    # show the html page that present all the days if it's a student
    if current_user.is_student:
        return render_template("day_menu.html", order_button_list=get_order_days(),
                               end_title='votre jour', is_student=True)
    else:
        print(current_user.classes)
        return render_template("day_menu.html", order_button_list=current_user.classes,
                               end_title='la classe', is_student=False)


@days_path.route('/days_button_clicked', methods=['POST'])
@login_required
def action_button_clicked():
    if not current_user.is_student:
        redirect(url_for('days_path.show_days'))

    day = request.form.get('day_button')

    time_left = already_voted(current_user.id, day)

    color_voted = sql_tools.get_color_voted(current_user.id, day)

    if color_voted is not None and time_left > 12:
        return redirect(url_for('revote_path.show_revote', day=day))
    elif time_left is None or time_left > 12:
        return redirect(url_for('vote_path.vote', day=day))
    else:
        return redirect(url_for('already_voted_path.show_already_voted', time_left=int(12 - time_left), day=day))


@days_path.route('/classe_button_clicked', methods=['POST'])
@login_required
def action_classe_clicked():
    if current_user.is_student:
        return redirect(url_for('days_path.show_days'))

    classe = request.form.get('day_button')
    order_button_list = get_order_days()

    color_per_day = {}

    for i in get_order_days():

        color = None

        match gradient_color(sql_tools.get_vote_per_class(classe, i)):
            case 'white':
                color = ['#ffffff', '#000000']
            case 'green':
                color = ['#04D600', '#000000']
            case 'yellow':
                color = ['#ffe600', '#000000']
            case 'orange':
                color = ['#ff9319', '#000000']
            case 'brown':
                color = ['#ad601f', '#ffffff']
            case 'red':
                color = ['#F80000', '#ffffff']
            case 'purple':
                color = ['#dd2ec0', '#ffffff']
            case 'black':
                color = ['#373737', '#ffffff']

        if color is None:
            color = ['#B8D7ED', '#000000']

        color_per_day[i] = color

    return render_template('days_classes_menu.html', classe=classe, order_button_list=order_button_list,
                           color_per_day=color_per_day)


@days_path.route('/days_classe_button', methods=['POST'])
@login_required
def action_day_button():
    button = request.form.get('day_button')

    if button == 'back':
        return redirect(url_for('days_path.show_days'))
    else:
        button = eval(button)
        return redirect(url_for('feeling_path.show_feeling', classe=button[1], day=button[0]))
