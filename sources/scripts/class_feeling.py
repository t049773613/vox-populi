from flask import Blueprint, render_template, redirect, url_for, request
from flask_login import login_required, current_user

from database import sql_tools
from scripts import tools

# create the Blueprint that allows to connect the root with the main app
feeling_path = Blueprint('feeling_path', __name__)


@feeling_path.route('/classe', methods=['GET'])
@login_required
def show_feeling():

    day = request.args.get('day')

    if current_user.is_student:

        time_left = tools.already_voted(current_user.id, day)

        if time_left is None:
            return redirect(url_for('vote_path.vote', day=day))
        elif time_left > 12:
            return redirect(url_for('revote_path.show_revote', day=day))

    if current_user.is_student:
        classe = sql_tools.get_class(current_user.id)
    else:
        classe = request.args.get('classe')
        print(classe)

    result = sql_tools.get_vote_per_class(classe, day)
    print(result)

    list_day = tools.get_order_days()
    if list_day[0] == str(day):
        left_arrow = False
        right_arrow = True
    elif list_day[-1] == str(day):
        right_arrow = False
        left_arrow = True
    else:
        right_arrow = True
        left_arrow = True

    return render_template('class_feeling.html', classe=classe, day=day, results=result,
                           left_arrow=left_arrow, right_arrow=right_arrow)


@feeling_path.route('/class_button_retour', methods=['POST'])
@login_required
def return_to_menu():
    return redirect(url_for('days_path.show_days'))


@feeling_path.route('/arrow_pressed', methods=['POST'])
@login_required
def arrow_action():

    left_arrow = request.form.get('left_arrow')
    right_arrow = request.form.get('right_arrow')

    if left_arrow is not None:
        left_arrow = eval(left_arrow)
        classe = left_arrow[1]
        left_arrow = left_arrow[0]
    else:
        right_arrow = eval(right_arrow)
        classe = right_arrow[1]
        right_arrow = right_arrow[0]

    list_day = tools.get_order_days()

    if right_arrow is not None:
        index_day = list_day.index(right_arrow)
        next_day = list_day[index_day + 1]
        return redirect(url_for('feeling_path.show_feeling', day=next_day, classe=classe))
    else:
        index_day = list_day.index(left_arrow)
        previous_day = list_day[index_day - 1]
        return redirect(url_for('feeling_path.show_feeling', day=previous_day, classe=classe))
    