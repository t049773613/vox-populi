from flask import Blueprint, request, render_template, redirect, url_for
from flask_login import login_required, current_user
from database import sql_tools
from scripts import tools

# create the Blueprint that allows to connect the root with the main app
revote_path = Blueprint('revote_path', __name__)


@revote_path.route('/revote', methods=['GET'])
@login_required
def show_revote():

    day = request.args.get('day')

    time_left = tools.already_voted(current_user.id, day)
    color_voted = sql_tools.get_color_voted(current_user.id, day)

    if color_voted is None or time_left < 12:
        return redirect(url_for('days_path.show_days'))

    color_voted = sql_tools.get_color_voted(current_user.id, day)
    color = None

    match color_voted:
        case 'green':
            color = 'background-color: #04D600;'
        case 'orange':
            color = 'background-color: #ffe600;'
        case 'red':
            color = 'background-color: #F80000;'
        case 'black':
            color = 'background-color: #373737;'

    print(color)

    return render_template('revote.html', color=color, day=day)


@revote_path.route('/revote_button', methods=['POST'])
@login_required
def action_button():

    revote_button = request.form.get('revote_button')
    feeling_button = request.form.get('feeling_button')

    if revote_button is not None and eval(revote_button)[0] == 'revote':
        print('revote')
        return redirect(url_for('vote_path.vote', day=eval(revote_button)[1]))
    elif feeling_button is not None and eval(feeling_button)[0] == 'feeling':
        print('feeling')

        match eval(feeling_button)[2]:
            case 'background-color: #04D600;':
                color = 'green'
            case 'background-color: #ffe600;':
                color = 'orange'
            case 'background-color: #F80000;':
                color = 'red'
            case 'background-color: #373737;':
                color = 'black'

        sql_tools.update_vote(current_user.id, color, eval(feeling_button)[1])
        return redirect(url_for('feeling_path.show_feeling', day=eval(feeling_button)[1]))
    else:
        print('back')
        return redirect(url_for('days_path.show_days'))
