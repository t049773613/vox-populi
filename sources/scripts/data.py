day_names = ["lundi", "mardi", "mercredi", "jeudi", "weekend"]
friday_day_names = ["weekend", "lundi", "mardi", "mercredi", "jeudi"]
thursday_day_names = ["jeudi", "weekend", "lundi", "mardi", "mercredi"]
wednesday_day_names = ["mercredi", "jeudi", "weekend", "lundi","mardi"]
tuesday_day_names = ["mardi", "mercredi", "jeudi", "weekend", "lundi"]
monday_day_names = ["lundi", "mardi","mercredi", "jeudi", "weekend"]

vote_colors = ['green', 'orange', 'red', 'black']

colors_weight = {
    0 : "white",
    2 : 'green',
    3 : 'yellow',
    4 : 'orange',
    5 : 'brown',
    6 : 'red',
    7 : 'purple',
    8 : 'black'
}

reverse_colors_weight = {
    'white' : 0,
    'green' : 2,
    'yellow' : 3,
    'orange' : 4,
    'brown' : 5,
    'red' : 6,
    'purple' : 7,
    'black' : 8
}