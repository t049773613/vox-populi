from flask import Blueprint, request, render_template, redirect, url_for
from flask_login import login_required, current_user

# create the Blueprint that allows to connect the root with the main app
already_voted_path = Blueprint('already_voted_path', __name__)


@already_voted_path.route('/already_voted', methods=['GET'])
@login_required
def show_already_voted():
    day = request.args.get('day')
    time_left = request.args.get('time_left')

    return render_template('already_voted.html', time_left=time_left, day=day)


@already_voted_path.route('/already_voted_button', methods=['POST'])
@login_required
def action_buttons_already_voted():

    feeling_button = request.form.get('feeling_button')

    if type(feeling_button) is str:
        return redirect(url_for('feeling_path.show_feeling', day=feeling_button))
    else:
        return redirect(url_for('days_path.show_days', time_left=10))
