from flask import Blueprint, redirect, url_for, request, render_template
from flask_login import login_required

# create the Blueprint that allows to connect the root with the main app
thanks_path = Blueprint('thanks_path', __name__)


@thanks_path.route('/thanks_vote', methods=['GET'])
@login_required
def show_thanks():

    day = request.args.get('day')

    return render_template('thanks.html', day=day)


@thanks_path.route('/thanks_vote_button', methods=['POST'])
@login_required
def action_button():
    if request.form.get('feel_button') is not None:
        return redirect(url_for('feeling_path.show_feeling', day=request.form.get('feel_button')))
    else:
        return redirect(url_for('days_path.show_days'))
