from flask_login import UserMixin


class User(UserMixin):
    """class that create the user infos"""

    def __init__(self, id, is_student, classes):
        self.id = id
        self.is_student = is_student
        self.classes = classes


class Config(object):
    SCHEDULER_API_ENABLED = True
