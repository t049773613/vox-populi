from flask import Blueprint, request, render_template, redirect, url_for
from flask_login import login_required, current_user
from scripts.tools import day_exists, already_voted
from database.sql_tools import update_vote, get_color_voted


# create the Blueprint that allows to connect the root with the main app
vote_path = Blueprint('vote_path', __name__)


@vote_path.route('/vote', methods=['GET'])
@login_required
def vote():
    """function that is called when a user wants to vote"""

    day = request.args.get('day')
    print(day)

    time_left = already_voted(current_user.id, day)

    color_voted = get_color_voted(current_user.id, day)

    if color_voted is not None and time_left > 12:
        return redirect(url_for('revote_path.show_revote', day=day))
    elif time_left is not None and time_left < 12:
        return redirect(url_for('already_voted_path.show_already_voted', time_left=int(12 - time_left), day=day))

    if not day_exists(day):
        return redirect(url_for('days_path.show_days'))

    if day == 'Weekend':
        day = 'ce weekend'

    return render_template('ask_charge.html', day=day)


@vote_path.route('/get_vote', methods=['POST'])
@login_required
def get_result_vote():
    """function that takes care of get the result of the vote"""

    if type(request.form.get('send_button')) == str:

        my_list = request.form.get('clicked_button')
        my_list = eval(my_list)

        if not isinstance(my_list, list) and len(my_list) <= 1:
            return redirect(url_for('days_path.show_days'))

        print(my_list)

        color = my_list[0]
        day = my_list[1]

        # test if the time is good

        # we had to define the id with the reel id first
        update_vote(current_user.id, color, day)

        return redirect(url_for('thanks_path.show_thanks', day=day))

    else:

        return redirect(url_for('days_path.show_days'))

