from flask import Blueprint, redirect, url_for, request
from flask_login import login_user

import requests
import json
import urllib.parse
import os

from scripts import user, tools
from database import sql_tools

# create the Blueprint that allows to connect the root with the main app
connection_path = Blueprint('connection_path', __name__)

# list of the test accounts
list_test_accounts = ['elevetest1', 'elevetest2', 'elevetest3', 'elevetest4', 'elevetest5',
                      'elevetest6', 'elevetest7', 'elevetest8', 'elevetest9', 'elevetest10',
                      'elevetest11', 'elevetest12', 'elevetest13', 'elevetest14', 'proftest1',
                      'proftest2', 'protest3']

def get_response_api_ecole_directe(login, password):
    """function that get the response of the ecole directe api"""

    # encoding strings to avoid errors
    login = urllib.parse.quote(login)
    password = urllib.parse.quote(password)

    # check if account is a test account :

    if login in list_test_accounts :

        return get_test_json(login)

    else :

        # create the url
        login_url = "https://api.ecoledirecte.com/v3/login.awp?v=4.26.3"  # url de login
        headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                             'Chrome/109.0.0.0 Safari/537.36'}

        payload = {"uuid": "", "identifiant": f"{login}", "motdepasse": f"{password}", "isReLogin": False}

        # get the response
        response = requests.request("POST", login_url, headers=headers, data="data=" + json.dumps(payload))

        print(response.json())

        return response.json()

def get_test_json (login):
    way_to_the_json = os.path.join("test_accounts", f"{login}.json")

    if os.path.exists(way_to_the_json):

        with open(way_to_the_json, 'r') as file:
            my_json = json.load(file)
        return my_json
    else:
        print(f"Le fichier pour le login '{login}' n'existe pas.")
        return None

def check_connection_info(response_api):
    """function that will check if the user with this login and this password exits"""

    # if it's a success
    if response_api['code'] == 200:
        return True

    # if it's a failed
    else:
        return False


def get_ecole_directe_name_last_name_classe(response_api):
    """function that return the name, last_name and classe of the user in ecole directe"""

    # gets the name from the response dictionary
    name = response_api['data']['accounts'][0]['prenom']

    # gets the last name from the response dictionary
    last_name = response_api['data']['accounts'][0]['nom']

    # gets the class from the response dictionary
    classe = response_api["data"]["accounts"][0]["profile"]["classe"]["code"]

    return name, last_name, classe


def get_ecole_directe_name_last_name_last_name_prof(response_api):
    # gets the name from the response api
    name = response_api['data']['accounts'][0]['prenom']

    # get the last_name from the response api
    last_name = response_api['data']['accounts'][0]['nom']

    return name, last_name


def get_ecole_directe_id(response_api):
    """function that return the id of the user in ecole directe"""

    # gets the id from the response dictionary
    return response_api['data']['accounts'][0]['id']


def get_ecole_directe_classes_teacher(response_api):
    """function that return the list of all the classes for a teacher"""

    list_teacher_classes = []
    api_classes = response_api["data"]["accounts"][0]["profile"]["classes"]

    for classe in api_classes:
        list_teacher_classes.append(classe['code'])

    print(list_teacher_classes)

    # gets the classes from the response dictionary
    return list_teacher_classes


@connection_path.route('/check_login', methods=['POST'])
def check_login():
    """function that check the login info"""

    # get the info that the form post
    login = request.form.get('login')
    password = request.form.get('password')

    # get the response of the ecole directe api
    api_data = get_response_api_ecole_directe(login, password)

    # check this infos
    if check_connection_info(api_data):

        print('connected')

        # get the name, last_name, classe, status, ecole directe id
        is_user_student = tools.is_student(api_data)

        if is_user_student:
            name, last_name, classe = get_ecole_directe_name_last_name_classe(api_data)
        else:
            name, last_name = get_ecole_directe_name_last_name_last_name_prof(api_data)
            classe = get_ecole_directe_classes_teacher(api_data)

        user_id = get_ecole_directe_id(api_data)

        # if the user is not in the database and if he is a student, add the user in the database
        if not sql_tools.is_in_db(user_id, 'lndb') and is_user_student:
            sql_tools.add_student(user_id, last_name, name, classe)
            print('student added')

        # if the teacher is not in the database and if he is a teacher, add the teacher in the database
        if not sql_tools.is_in_db(user_id, 'professeurs') and not is_user_student:
            sql_tools.add_teacher(user_id, last_name, name, classe)
            print('teacher added')

        # get the list of classes if it's a teacher
        if not is_user_student:
            classe = sql_tools.get_list_of_classes(user_id)

        # saved the session_user
        user_instance = user.User(id=user_id, is_student=is_user_student, classes=classe)
        login_user(user_instance, remember=True)

        # redirect to the next window
        return redirect(url_for('days_path.show_days'))

    # if the connection infos are false
    else:

        # create an error message
        error_message = "Identifiant ou mot de passe incorrect."
        print('error')

        # redirect to the main root with an error message
        return redirect(url_for('start', error_message=error_message))
