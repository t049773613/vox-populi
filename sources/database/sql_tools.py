import sqlite3
import time
from scripts import tools


def sql_execute(code, request=None):
    result = None
    # connect to the sql file
    connection = sqlite3.connect('users.db')
    # defined a cursor and execute a command
    c = connection.cursor()
    c.execute(code)
    if request is not None:
        if request == "fetchone":
            result = c.fetchone()
        if request == "fetchall":
            result = c.fetchall()
    connection.commit()
    connection.close()
    return result


def is_in_db(user_id, table):
    result = sql_execute(f'''SELECT id FROM {table} ''', "fetchall")
    user_id = (int(user_id),)

    if user_id in result:
        return True
    else:
        return False


def update_vote(edited_id, color, day):
    day_hour = day
    match day_hour:
        case 'lundi':
            day_hour = 'monday_hour'
        case 'mardi':
            day_hour = 'tuesday_hour'
        case 'mercredi':
            day_hour = 'wednesday_hour'
        case 'jeudi':
            day_hour = 'thursday_hour'
        case 'weekend':
            day_hour = 'weekend_hour'

    match day:
        case 'lundi':
            day = 'monday_vote'
        case 'mardi':
            day = 'tuesday_vote'
        case 'mercredi':
            day = 'wednesday_vote'
        case 'jeudi':
            day = 'thursday_vote'
        case 'weekend':
            day = 'weekend_vote'

    code = f" UPDATE lndb SET {day} = '{color}', {day_hour} = '{time.time()}' WHERE id = '{edited_id}'"

    sql_execute(code)


def add_student(id, new_lastname, new_name, new_class):
    sql_execute(f''' INSERT INTO lndb(id, last_name, name, classe) VALUES('{id}', '{new_lastname}', '{new_name}', '{new_class}')''')


def get_vote_per_class(wanted_class, day):
    match day:
        case 'lundi':
            day = 'monday_vote'
        case 'mardi':
            day = 'tuesday_vote'
        case 'mercredi':
            day = 'wednesday_vote'
        case 'jeudi':
            day = 'thursday_vote'
        case 'weekend':
            day = 'weekend_vote'
    result = sql_execute(f''' SELECT {day} FROM lndb WHERE classe = '{wanted_class}' ''', "fetchall")

    result_to_return = {'green': 0, 'orange': 0, 'red': 0, 'black': 0}

    for i in result:
        if i == ('green',):
            result_to_return['green'] += 1
        elif i == ('orange',):
            result_to_return['orange'] += 1
        elif i == ('red',):
            result_to_return['red'] += 1
        elif i == ('black',):
            result_to_return['black'] += 1

    return result_to_return


def get_class(edited_id):
    result = sql_execute(f''' SELECT classe
            FROM lndb
            WHERE id = "{edited_id}"''', "fetchone")

    return result[0]


def add_teacher(user_id, name, last_name, list_of_classes):
    """function that takes care of adding a new teacher in the teacher table"""
    sql_execute('''INSERT INTO professeurs(id, last_name, name, classes) VALUES("{}", "{}", "{}", "{}")'''.format(str(user_id), str(last_name), str(name), str(tools.list_to_string(list_of_classes))))


def get_list_of_classes(teacher_id):
    result = sql_execute(f'''SELECT classes FROM professeurs WHERE id = "{teacher_id}"''', "fetchone")
    if result is not None:
        result = result[0]
        result = tools.str_to_list(result)
    return result


def get_time_for_vote(user_id, day):
    """function that return the time when we voted for a day"""

    match day:
        case 'lundi':
            day = 'monday_hour'
        case 'mardi':
            day = 'tuesday_hour'
        case 'mercredi':
            day = 'wednesday_hour'
        case 'jeudi':
            day = 'thursday_hour'
        case 'weekend':
            day = 'weekend_hour'

    result = sql_execute(f'''SELECT {day} FROM lndb WHERE id = "{user_id}"''', "fetchone")
    if result is not None:
        result = result[0]
    return result


def get_color_voted(user_id, day):
    """function that return the color voted by a user for a special day"""

    match day:
        case 'lundi':
            day = 'monday_vote'
        case 'mardi':
            day = 'tuesday_vote'
        case 'mercredi':
            day = 'wednesday_vote'
        case 'jeudi':
            day = 'thursday_vote'
        case 'weekend':
            day = 'weekend_vote'

    result = sql_execute(f'''SELECT {day} FROM lndb WHERE id = "{user_id}"''', "fetchone")
    if result is not None:
        result = result[0]
    return result


def delete_vote_end_day():
    # get the day that we want to delete
    day_to_delete_vote = tools.get_order_days()[0]

    match day_to_delete_vote:
        case 'lundi':
            vote_column = 'monday_vote'
            hour_column = 'monday_hour'
        case 'mardi':
            vote_column = 'tuesday_vote'
            hour_column = 'tuesday_hour'
        case 'mercredi':
            vote_column = 'wednesday_vote'
            hour_column = 'wednesday_hour'
        case 'jeudi':
            vote_column = 'thursday_vote'
            hour_column = 'thursday_hour'
        case 'weekend':
            vote_column = 'weekend_vote'
            hour_column = 'weekend_hour'

    sql_execute(f'''UPDATE lndb SET {vote_column} = NULL''')
    sql_execute(f'''UPDATE lndb SET {hour_column} = NULL''')

    return
