
# Ce fichier sert à la création d'un database 'users' qui contient une unique table vide appelée 'lndb'.
# NE APS EXECUTER SI 'users.db' EXISTE DEJA !


import sqlite3

connection = sqlite3.connect("../users.db")
c = connection.cursor()

c.execute('''CREATE TABLE IF NOT EXISTS lndb \
            (id string, \
             last_name string, \
             name string, \
             classe string, \
             monday_vote string, \
             tuesday_vote string, \
             wednesday_vote string, \
             thursday_vote string, \
             weekend_vote string, \
             monday_hour float, \
             tuesday_hour float, \
             wednesday_hour float, \
             thursday_hour float, \
             weekend_hour float
             )''')

c.execute("""CREATE TABLE IF NOT EXISTS professeurs (id string, last_name string, name string, classes string)""")

connection.commit()
connection.close()