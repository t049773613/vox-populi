from flask import Flask, render_template, request
from flask_login import LoginManager, logout_user
from flask_apscheduler import APScheduler

from scripts import connection, user, days, vote, thanks, class_feeling, already_voted, revote
from database import sql_tools

# create the app for flask
app = Flask(__name__)
app.config['SECRET_KEY'] = 'hjshjhdjah kjshkjdhjs  dbgdgdgdgdg'

# defined the login_manager system
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = "start"


# defined the task planification
app.config.from_object(user.Config())
scheduler = APScheduler()
scheduler.init_app(app)
scheduler.start()


@login_manager.user_loader
def load_user(user_id):
    """function that allows login_manager to get the user_info"""

    is_student = sql_tools.is_in_db(user_id, 'lndb')

    if is_student:
        classes = None
    else:
        classes = sql_tools.get_list_of_classes(user_id)

    return user.User(user_id, is_student=is_student, classes=classes)


# the main root of the app
@app.route('/', methods=['GET'])
def start():
    """function that is called when we are at the main root of the web application"""

    # disconnect the user if he is connected
    logout_user()

    # we check if we have anu error message
    error = request.args.get('error_message')

    # we show the html file with the error message if it exists
    return render_template("connection.html", error_message=error)


# add the root of the checking connection
app.register_blueprint(connection.connection_path)

# add the root of the page with all the days
app.register_blueprint(days.days_path)

# add the root of the page vote
app.register_blueprint(vote.vote_path)

# add the root of the thanks page
app.register_blueprint(thanks.thanks_path)

# add the root of the class_feeling
app.register_blueprint(class_feeling.feeling_path)

# add the root of the already_voted
app.register_blueprint(already_voted.already_voted_path)

# add the root of the revote
app.register_blueprint(revote.revote_path)


# add a task
@scheduler.task('cron', id='reinitialise_last_day', hour=23, minute=59)
def scheduled_task():
    sql_tools.delete_vote_end_day()


if __name__ == '__main__':
    app.run(debug=True)
