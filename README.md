# Vox Populi
Ce projet a été fait à l'occasion du concours Trophées NSI 2024 réalisé par des élèves de terminale NSI du lycée Notre-Dame de Boulogne, accompagnés de leur professeur de NSI. Il répond à la problématique de la charge de travail des élèves souvent mal évaluée par les professeurs. 
Les élèves ont parfois des devoirs importants à réaliser dans un délai très court ou bien peu de travail sur une longue période. 
Pour authentifier les élèves, nous avons utilisé les identifiants Ecole Directe (et bientôt Pronote). Nous avons codé un module supplémentaire sous la forme d'une application web via Flask. Celle-ciauthentifie les élèves (sans sauvegarder leurs identifiants) et leur propose de voter selon 4 couleurs : 
    
- Vert, la charge de travail estimé par l'élève, pour le soir, est peu importante voire inexistante,
- Jaune, l'élève estime que sa charge est faible mais nécessite de s’y consacrer,
- Rouge, la charge de travail est importante et l'élève signale aux professeurs qu'ils ne doivent pas lui en rajouter,
- Noir, l'élève est submergé et sa charge de travail est excessive.

L'interface se compose d'une page de connexion puis d'un affichage de 5 boutons : Lundi, Mardi, Mercredi, Jeudi et Weekend qui, une fois cliqué, 
amène à la fenêtre de vote puis enfin à un diagramme de tous les ressentis des autres élèves. Les professeurs, qui ont une interface propre, 
peuvent ainsi consultés le ressenti général de la classe. Ce projet repose sur l'honnêteté des élèves sur leur ressenti et la prise en compte 
par les professeurs de ces même ressentis. L'élève dispose d'un vote par jour et un délai de 12h entre chaque vote. Il peut ainsi revoter si sa charge 
de travail a augmenté ou diminué.


# Lancement de l'application

Sous windows, Linux et MacOs : Lancez successivement les commandes 
    "cd sources" et "python main.py"



